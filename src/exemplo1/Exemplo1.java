/**
 * Exemplo1: Programacao com threads
 * Autor: Lucio Agostinho Rocha
 * Willian Alberto Lauber
 * Ultima modificacao: 07/08/2017
 */

package exemplo1;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Lucio
 * @author Willian Alberto Lauber
 */
public class Exemplo1  {

    public static void main(String [] args){
        Thread t;

        System.out.println("Inicio da criacao das threads.");

        ExecutorService executor = Executors.newCachedThreadPool();
        for (int i = 0; i < 20; i++) {
            Runnable worker = new PrintTasks("thread:", i);
            executor.execute(worker);
          }
        executor.shutdown();
        while (!executor.isTerminated()) { }
        System.out.println("Hasta la vista threads");
    }       
}
